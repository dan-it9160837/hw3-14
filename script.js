// 1. В чому полягає відмінність localStorage і sessionStorage?

// Це обидві технології для збереження даних в браузері

// Дані, збережені в localStorage, залишаються в браузері навіть після його закриття та перезапуску комп'ютера.
// Дані, збережені в sessionStorage, діють лише протягом поточного сеансу браузера. 

// 2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?

// Слід ніколи не зберігате паролі в чистому вигляді, Захист від XSS-атак, Захист від CSRF-атак, Обмеження доступу до інформації, Використовуйте безпечні (HTTPS) з'єднання.

// 3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?


// Коли сеанс браузера завершується, дані, збережені в sessionStorage, видаляються. sessionStorage призначений для тимчасового збереження даних, і його область видимості обмежена сесією браузера.

// Реалізувати можливість зміни колірної теми користувача.

const button = document.getElementById('change-color-button');
const main = document.querySelector('main');

let currentColor = 'yellow';

if (localStorage.getItem("currentColor")) {
  currentColor = localStorage.getItem("currentColor");
  main.style.backgroundColor = currentColor;
}

function toggleColor() {
  if (currentColor === 'yellow') {
    main.style.backgroundColor = '#EEEEEF';
    currentColor = '#EEEEEF';
    localStorage.setItem("currentColor", currentColor);
  } else {
    main.style.backgroundColor = 'yellow';
    currentColor = 'yellow';
    localStorage.setItem("currentColor", currentColor);
  }
}

button.addEventListener('click', toggleColor);